package com.stefanini.taskmanager.service.implementation;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.dao.factory.AbstractDAOFactory;
import com.stefanini.taskmanager.dao.factory.DAOFactory;
import com.stefanini.taskmanager.dao.factory.FactoryType;
import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import com.stefanini.taskmanager.service.contract.TaskService;
import com.stefanini.taskmanager.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.stefanini.taskmanager.util.ArgsReader.*;


public class TaskServiceImpl implements TaskService {
    private static final Logger log = Logger.getLogger(TaskServiceImpl.class);
    private final UserDAO userDAO;
    private final TaskDAO taskDAO;

    public TaskServiceImpl() {
        DAOFactory daoFactory = AbstractDAOFactory.createDAOFactory(FactoryType.HIBERNATE);
        this.userDAO = daoFactory.getUserDAO();
        this.taskDAO = daoFactory.getTaskDAO();
    }

    @Loggable
    @Override
    public Task createEntity(String[] args) {
        log.info("Creating task ...");
        Transaction tx = null;
        Task newTask = parseArgsForTask(args);
        try (Session session = HibernateUtil.getSession()) {
            if (Objects.nonNull(taskDAO.getRecordByUniqueName(newTask.getTaskTitle(), session))) {
                log.warn("User " + newTask.getTaskTitle() + " already exists");
                return taskDAO.getRecordByUniqueName(newTask.getTaskTitle(), session);
            }
            tx = session.beginTransaction();
            newTask = taskDAO.createRecord(newTask, session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
        return newTask;
    }

    @Loggable
    @Override
    public List<Task> getEntityList() {
        log.info("Creating list of all tasks ...");
        List<Task> taskList = null;
        try (Session session = HibernateUtil.getSession()) {
            taskList = taskDAO.getRecordsList(session);
            if (taskList.size() == 0) {
                log.warn("List of tasks is empty! No tasks created!");
            } else {
                taskList.forEach(log::info);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    @Loggable
    @Override
    public void deleteAll() {
        log.info("Deleting all tasks ...");
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            taskDAO.deleteAllRecords(session);
            log.info("All tasks deleted!");
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public Task deleteEntity(String[] args) {
        log.info("Deleting concrete task ...");
        Task task = null;
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            task = taskDAO.deleteRecordByUniqueName(createParameters(args, TT_LABEL), session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
        return task;
    }

    @Loggable
    @Override
    public List<Task> getTasksByUsername(String[] args) {
        log.info("Searching for list of tasks assigned to user " + createParameters(args, UN_LABEL) + " ...");
        List<Task> userTasks = new ArrayList<>();
        Transaction tx;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            userTasks = taskDAO.getTasksByUsername(createParameters(args, UN_LABEL), session);
            if (userTasks.size() == 0) {
                log.warn("No tasks were assigned to this user");
            } else {
                userTasks.forEach(log::info);
            }
            tx.commit();
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userTasks;
    }

    @Loggable
    @Override
    public void assignTaskToUser(String[] args) {
        log.info("Assigning task to user ...");
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            User user = userDAO.getRecordByUniqueName(createParameters(args, UN_LABEL), session);
            Task task = taskDAO.getRecordByUniqueName(createParameters(args, TT_LABEL), session);
            if (Objects.isNull(user) || Objects.isNull(task)) {
                log.warn("Can't assign. User or Task is null");
                return;
            }
            taskDAO.assignTask(user, task, session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public void removeTaskFromUser(String[] args) {
        log.info("Removing concrete task from user ...");
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            User user = userDAO.getRecordByUniqueName(createParameters(args, UN_LABEL), session);
            Task task = taskDAO.getRecordByUniqueName(createParameters(args, TT_LABEL), session);
            if (Objects.isNull(user) || Objects.isNull(task)) {
                log.error("Can't remove. User or Task is null");
                return;
            }
            taskDAO.completeTask(user, task, session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public void assignNewTaskToNewUser(String[] args) {
        log.info("Assigning new task to new user ...");
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            User user = userDAO.createRecord(parseArgsForUser(args), session);
            Task task = taskDAO.createRecord(parseArgsForTask(args), session);
            taskDAO.assignTask(user, task, session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }
}