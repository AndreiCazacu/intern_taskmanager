package com.stefanini.taskmanager.service.contract;

import com.stefanini.taskmanager.entity.Task;

import java.util.List;

public interface TaskService extends CommonService<Task> {

    /**
     * Displays list of entities fetched by DAO for user with params mentioned in args
     *
     * @param args arguments passed to app from command line
     * @return List of Tasks
     */
    List<Task> getTasksByUsername(String[] args);

    /**
     * Assigns task mentioned in params to user mentioned in params
     *
     * @param args arguments passed to app from command line
     */
    void assignTaskToUser(String[] args);

    /**
     * Assigns new task mentioned in params to new user mentioned in params
     *
     * @param args arguments passed to app from command line
     */
    void assignNewTaskToNewUser(String[] args);

    /**
     * Removes task mentioned in params to user mentioned in params
     *
     * @param args arguments passed to app from command line
     */
    void removeTaskFromUser(String[] args);

}