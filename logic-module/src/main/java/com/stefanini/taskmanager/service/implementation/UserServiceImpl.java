package com.stefanini.taskmanager.service.implementation;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.dao.factory.AbstractDAOFactory;
import com.stefanini.taskmanager.dao.factory.DAOFactory;
import com.stefanini.taskmanager.dao.factory.FactoryType;
import com.stefanini.taskmanager.entity.User;
import com.stefanini.taskmanager.service.contract.UserService;
import com.stefanini.taskmanager.util.HibernateUtil;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Objects;

import static com.stefanini.taskmanager.util.ArgsReader.*;

public class UserServiceImpl implements UserService {
    private static final Logger log = Logger.getLogger(UserServiceImpl.class);
    private final UserDAO userDAO;
    private final TaskDAO taskDAO;

    public UserServiceImpl () {
        DAOFactory daoFactory = AbstractDAOFactory.createDAOFactory(FactoryType.HIBERNATE);
        this.userDAO = daoFactory.getUserDAO();
        this.taskDAO = daoFactory.getTaskDAO();
    }

    @Loggable
    @Override
    public User createEntity(String[] args) {
        log.info("Creating user ...");
        Transaction tx = null;
        User newUser = parseArgsForUser(args);
        try (Session session = HibernateUtil.getSession()) {
            if (Objects.nonNull(userDAO.getRecordByUniqueName(newUser.getUsername(), session))) {
                log.warn("User " + newUser.getUsername() + " already exists");
                return userDAO.getRecordByUniqueName(newUser.getUsername(), session);
            }
            tx = session.beginTransaction();
            newUser = userDAO.createRecord(newUser, session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
        return newUser;
    }

    @Loggable
    @Override
    public List<User> getEntityList() {
        log.info("Creating list of all users ...");
        List<User> userList = null;
        try (Session session = HibernateUtil.getSession()) {
            userList = userDAO.getRecordsList(session);
            if (userList.size() == 0) {
                log.warn("List of users is empty! No users created!");
            } else {
                userList.forEach(log::info);
            }
        } catch (HibernateException e) {
            e.printStackTrace();
        }
        return userList;
    }

    @Loggable
    @Override
    public void deleteAll() {
        log.info("Deleting all users ...");
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            userDAO.deleteAllRecords(session);
            log.info("All users deleted!");
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public User deleteEntity(String[] args) {
        log.info("Deleting concrete user ...");
        User user = null;
        Transaction tx = null;
        try (Session session = HibernateUtil.getSession()) {
            tx = session.beginTransaction();
            user = userDAO.deleteRecordByUniqueName(createParameters(args, UN_LABEL), session);
            tx.commit();
        } catch (HibernateException e) {
            if (Objects.nonNull(tx)) {
                try {
                    log.error("Transaction rollback due to " + e.getMessage());
                    tx.rollback();
                } catch (HibernateException ex) {
                    log.error("Can't rollback transaction due to error - " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
            e.printStackTrace();
        }
        return user;
    }
}
