package com.stefanini.taskmanager.service.contract;

import java.util.List;

public interface CommonService<T> {

    /**
     * Creates T instance, maps fields with extracted params, and call DAO entity
     *
     * @param args arguments passed to app from command line
     * @return T instance, created entity
     */
    T createEntity(String[] args);

    /**
     * Displays list of entities fetched by DAO
     *
     * @return List, list of entities
     */
    List<T> getEntityList();

    /**
     * Deletes list of entities fetched by DAO
     */
    void deleteAll();

    /**
     * Deletes entity fetched by DAO
     *
     * @param args arguments passed to app from command line, by this param entity will be found
     * @return T instance, deleted entity
     */
    T deleteEntity(String[] args);

}
