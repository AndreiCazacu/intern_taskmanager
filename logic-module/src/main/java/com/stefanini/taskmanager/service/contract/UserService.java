package com.stefanini.taskmanager.service.contract;

import com.stefanini.taskmanager.entity.User;

public interface UserService extends CommonService<User> {

}
