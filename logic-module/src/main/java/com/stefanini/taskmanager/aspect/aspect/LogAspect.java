package com.stefanini.taskmanager.aspect.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class LogAspect {
    private static final Logger log = Logger.getLogger(LogAspect.class);

    @Pointcut("execution(* *(..)) && @annotation(com.stefanini.taskmanager.aspect.contract.Loggable)")
    public void logMethod() {
    }

    @Before("logMethod()")
    public void logMethodBefore(JoinPoint jP) {

        log.info("DEMONSTRATION BEFORE: " + jP.getThis().getClass().getSimpleName() + " - [" + jP.getSignature() + "]");
    }

    @After("logMethod()")
    public void logMethodAfter(JoinPoint jP) {

        log.info("DEMONSTRATION AFTER: " + jP.getThis().getClass().getSimpleName() + " - [" + jP.getSignature() + "]");
    }
}