package com.stefanini.taskmanager.aspect.aspect;

import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import com.stefanini.taskmanager.util.MailConnector;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Aspect
public class MailAspect {
    private static final Logger log = Logger.getLogger(MailAspect.class);

    @AfterReturning("execution(* *(..)) && @annotation(com.stefanini.taskmanager.aspect.contract.Notifiable)")
    public void sendMail(JoinPoint jP) {
        try {
            User user = (User) jP.getArgs()[0];
            Task task = (Task) jP.getArgs()[1];
            String mailSubject = "Notification about creating user and assigning task";
            String mailContent = "User (" + user.getFirstName() + ") / {" + user.getLastName() + "} identified by {"
                    + user.getUsername() + "} has been created.\n" + "Task {" + task.getTaskTitle() + "} {"
                    + task.getTaskDescription() + "} has been assigned to {" + user.getUsername() + "}";

            MailConnector mailSender = new MailConnector();

            MimeMessage message = new MimeMessage(mailSender.getSession());
            message.setFrom(new InternetAddress(mailSender.getMailFrom()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(mailSender.getMailTo()));
            message.setSubject(mailSubject);
            message.setText(mailContent);

            mailSender.sendMail(message);
        } catch (Throwable thr) {
            log.error("DEMONSTRATION AROUND: " + jP.getThis().getClass().getSimpleName() + " - [" + jP.getSignature() + "]");
            thr.printStackTrace();
        }
    }
}
