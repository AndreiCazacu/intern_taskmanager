package com.stefanini.taskmanager.dao.factory;

import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;

public interface DAOFactory {

    UserDAO getUserDAO();
    TaskDAO getTaskDAO();

}
