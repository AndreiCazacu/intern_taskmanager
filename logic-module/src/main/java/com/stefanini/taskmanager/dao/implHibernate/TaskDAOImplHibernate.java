package com.stefanini.taskmanager.dao.implHibernate;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.aspect.contract.Notifiable;
import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

public class TaskDAOImplHibernate implements TaskDAO {
    private static final Logger log = Logger.getLogger(TaskDAOImplHibernate.class);
    private final UserDAO userDAO = UserDAOImplHibernate.getInstance();
    private static TaskDAOImplHibernate instance;
    private Class<Task> persistentTask;
    private String className;

    {
        setPersistentClass(Task.class);
    }

    private TaskDAOImplHibernate() {

    }

    public static synchronized TaskDAOImplHibernate getInstance() {
        if (instance == null) {
            instance = new TaskDAOImplHibernate();
        }
        return instance;
    }

    @Loggable
    @Override
    public Task createRecord(Task task, Session session) throws HibernateException {
        log.info("Creating new " + className);
        Long newId = (Long) session.save(task);
        return getRecordById(newId, session);
    }

    @Loggable
    @Override
    public List<Task> getRecordsList(Session session) throws HibernateException {
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteria = builder.createQuery(persistentTask);
        Root<Task> root = criteria.from(persistentTask);
        criteria.select(root);
        return session.createQuery(criteria).getResultList();
    }

    @Loggable
    @Override
    public void deleteAllRecords(Session session) throws HibernateException {
        log.info("Deleting all " + className + " records");
        Query query = session.createQuery("delete from Task");
        int rowsAffected = query.executeUpdate();
        if (rowsAffected > 0) {
            log.info("Deleted " + rowsAffected + " records.");
        }
    }

    @Loggable
    @Override
    public Task getRecordByUniqueName(String taskTitle, Session session) throws HibernateException {
        log.info("Searching for task with title " + taskTitle);
        Task task;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteria = builder.createQuery(persistentTask);
        Root<Task> root = criteria.from(persistentTask);
        criteria.select(root);
        List<Task> tasks = session.createQuery(criteria).getResultList();
        task = tasks.stream().filter(tsk->tsk.getTaskTitle().equals(taskTitle)).findFirst().orElse(null);
        return task;
    }

    @Loggable
    @Override
    public Task deleteRecordByUniqueName(String taskTitle, Session session) throws HibernateException {
        Task task = getRecordByUniqueName(taskTitle, session);
        if (Objects.nonNull(task)) {
            log.info("Deleting " + className + " with title " + taskTitle);
            session.delete(task);
            log.info(className + " with title " + taskTitle + " was deleted");
        } else {
            log.warn("Task " + taskTitle + " does not exist.");
        }
        return task;
    }

    @Loggable
    @Override
    public Task getRecordById(Long id, Session session) throws HibernateException {
        log.info("Searching for " + className + " with id " + id);
        return session.get(persistentTask, id);
    }

    @Loggable
    @Override
    public Task deleteRecordById(Long id, Session session) throws HibernateException {
        Task task = getRecordById(id, session);
        if (Objects.nonNull(task)) {
            log.info("Deleting " + className + " with Id= " + id);
            session.delete(task);
            log.info(className + " with Id=" + id + " was deleted");
        } else {
            log.warn("Task with Id= " + id + " does not exist.");
        }
        return task;
    }

    @Loggable
    @Override
    public List<Task> getTasksByUsername(String username, Session session) throws HibernateException {
        User user = userDAO.getRecordByUniqueName(username, session);
        log.info ("Creating tasks list...");
        return new ArrayList<>(user.getTasks());
    }

    @Loggable
//    @Notifiable
    @Override
    public void assignTask(User user, Task task, Session session) throws HibernateException {
        user.addTask(task);
        session.saveOrUpdate(user);
        log.info("Assigning task to user " + user.getUsername());
    }

    @Loggable
    @Override
    public void completeTask(User user, Task task, Session session) throws HibernateException {
        if (user.getTasks().contains(task)) {
            user.removeTask(task);
            session.saveOrUpdate(user);
            log.info("Task " + task + " has been removed.");
        } else {
            log.info(user + "does not have task " + task);
        }
    }

    private void setPersistentClass(Class<Task> task) {
        this.persistentTask = task;
        this.className = persistentTask == null ? "NoName" : persistentTask.getName();
    }

    private Class<Task> getPersistentClass() {
        return persistentTask;
    }
}