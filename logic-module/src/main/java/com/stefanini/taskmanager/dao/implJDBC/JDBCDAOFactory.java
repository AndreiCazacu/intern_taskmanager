package com.stefanini.taskmanager.dao.implJDBC;

import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.dao.factory.DAOFactory;

public class JDBCDAOFactory implements DAOFactory {

    @Override
    public UserDAO getUserDAO() {
        return UserDAOImplJDBC.getInstance();
    }

    @Override
    public TaskDAO getTaskDAO() {
        return TaskDAOImplJDBC.getInstance();
    }
}
