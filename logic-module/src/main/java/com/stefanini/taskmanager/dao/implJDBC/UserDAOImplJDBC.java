package com.stefanini.taskmanager.dao.implJDBC;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.util.DBConnector;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.entity.User;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserDAOImplJDBC implements UserDAO {
    private static final Logger log = Logger.getLogger(UserDAOImplJDBC.class);
    private static UserDAOImplJDBC instance;

    private UserDAOImplJDBC() {

    }

    public static synchronized UserDAOImplJDBC getInstance() {
        if (instance == null) {
            instance = new UserDAOImplJDBC();
        }
        return instance;
    }

    @Loggable
    @Override
    public User createRecord(User user, Session session) {
        String firstName = user.getFirstName();
        String lastName = user.getLastName();
        String username = user.getUsername();
        String query = "INSERT INTO users (First_Name, Last_Name, Username) VALUES(?,?,?)";
        long newId = 0L;

        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            log.info("Creating new user - " + username);
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setString(3, username);
            statement.executeUpdate();
            try (ResultSet result = statement.getGeneratedKeys()) {
                if (result.next()) {
                    newId = result.getLong(1);
                }
            }
        } catch (SQLException e) {
            log.error("User was not created due to error - " + e.getMessage());
            e.printStackTrace();
        }
        return getRecordById(newId, session);
    }

    @Loggable
    @Override
    public List<User> getRecordsList(Session session) {
        String query = "SELECT * FROM users";
        List<User> usersList = new ArrayList<>();

        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    usersList.add(new User(result.getLong("Id"),
                            result.getString("First_Name"),
                            result.getString("Last_Name"),
                            result.getString("Username")));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get all users list due to - " + e.getMessage());
            e.printStackTrace();
        }
        return usersList;
    }

    @Loggable
    @Override
    public void deleteAllRecords(Session session) {
        String query = "DELETE users, users_tasks FROM users LEFT JOIN users_tasks ON users.Id = users_tasks.Id_User";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Can't delete all users due to - " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public User getRecordByUniqueName(String username, Session session) {
        User user = null;
        String query = "SELECT * FROM users WHERE Username = ?";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, username);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    user = new User(result.getLong("Id"),
                            result.getString("First_Name"),
                            result.getString("Last_Name"),
                            result.getString("Username"));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get user due to - " + e.getMessage());
            e.printStackTrace();
        }
        return user;
    }

    @Loggable
    @Override
    public User deleteRecordByUniqueName(String username, Session session) {
        User user = getRecordByUniqueName(username, session);
        if (Objects.nonNull(user)) {
            String query = "DELETE FROM users WHERE Username= ?";
            try {
                Connection connection = DBConnector.getUniqueConnection();
                PreparedStatement statement = connection.prepareStatement(query);

                deleteLinkUserTask(user, connection);

                statement.setString(1, username);
                statement.executeUpdate();
                log.info("User " + username + " was deleted");
            } catch (SQLException e) {
                log.error("User was not deleted due to error - " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.warn("User " + username + " does not exist.");
        }
        return user;
    }

    @Loggable
    @Override
    public User getRecordById(Long id, Session session) {
        User user = null;
        String query = "SELECT * FROM users WHERE Id = ?";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    user = new User(result.getLong("Id"),
                            result.getString("First_Name"),
                            result.getString("Last_Name"),
                            result.getString("Username"));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get user due to - " + e.getMessage());
            e.printStackTrace();
        }
        return user;
    }

    @Loggable
    @Override
    public User deleteRecordById(Long id, Session session) {
        User user = getRecordById(id, session);
        if (Objects.nonNull(user)) {
            String query = "DELETE FROM users WHERE Id= ?";
            try {
                Connection connection = DBConnector.getUniqueConnection();
                PreparedStatement statement = connection.prepareStatement(query);

                deleteLinkUserTask(user, connection);

                statement.setLong(1, id);
                statement.executeUpdate();
                log.info("User with Id= " + id + " was deleted");
            } catch (SQLException e) {
                log.error("User was not deleted due to error - " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.warn("User with Id= " + id + " does not exist.");
        }
        return user;
    }


    private void deleteLinkUserTask(User user, Connection connection) {
        String linkQuery = "DELETE FROM users_tasks WHERE Id_User= ?";
        try {
            PreparedStatement statement = connection.prepareStatement(linkQuery);
            statement.setLong(1, user.getId());
            statement.executeUpdate();
            log.info("Links with tasks for user " + user + " has been deleted from table users_tasks");
        } catch (SQLException e) {
            try {
                log.error("Transaction rollback due to " + e.getMessage());
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
                log.error(ex.getMessage());
            }
        }
    }
}