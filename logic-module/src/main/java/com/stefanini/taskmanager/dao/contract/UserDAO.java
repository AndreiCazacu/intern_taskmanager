package com.stefanini.taskmanager.dao.contract;

import com.stefanini.taskmanager.entity.User;

public interface UserDAO extends AbstractDAO<User> {

}
