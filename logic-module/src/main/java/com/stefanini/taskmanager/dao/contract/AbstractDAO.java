package com.stefanini.taskmanager.dao.contract;

import org.hibernate.Session;

import java.util.List;

public interface AbstractDAO<T> {

    /**
     * Creates new entity, inserts as a record into the table, and return it
     *
     * @param entity entity-param to be inserted as a record
     * @param session Hibernate session for Hibernate implementation
     * @return created entity
     */
    T createRecord (T entity, Session session);

    /**
     * Creates a list of all records from the table, and return it as a list of entities
     *
     * @param session Hibernate session for Hibernate implementation
     * @return entities list, list of instances
     */
    List<T> getRecordsList(Session session);

    /**
     * Deletes all records from the table
     * @param session Hibernate session for Hibernate implementation
     */
    void deleteAllRecords(Session session);

    /**
     * Searches record in the table by param, and fetches it as entity
     *
     * @param uniqueName String uniqueName-param used for search
     * @param session Hibernate session for Hibernate implementation
     * @return entity, found in the table
     */
    T getRecordByUniqueName(String uniqueName, Session session);

    /**
     * Deletes record in the table by param, and fetches it as entity
     *
     * @param uniqueName String uniqueName-param used for delete
     * @param session Hibernate session for Hibernate implementation
     * @return deleted entity
     */
    T deleteRecordByUniqueName(String uniqueName, Session session);

    /**
     * Searches record in the table by id, and fetches it as entity
     *
     * @param id Long id-param used for search
     * @param session Hibernate session for Hibernate implementation
     * @return entity, found in the table
     */
    T getRecordById(Long id, Session session);

    /**
     * Deletes record in the table by id, and fetches it as entity
     *
     * @param id Long id-param used for delete
     * @param session Hibernate session for Hibernate implementation
     * @return deleted entity
     */
    T deleteRecordById(Long id, Session session);

}