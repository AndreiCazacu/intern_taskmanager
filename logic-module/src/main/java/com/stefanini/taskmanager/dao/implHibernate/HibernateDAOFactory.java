package com.stefanini.taskmanager.dao.implHibernate;

import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.dao.factory.DAOFactory;

public class HibernateDAOFactory implements DAOFactory {
    @Override
    public UserDAO getUserDAO() {
        return UserDAOImplHibernate.getInstance();
    }

    @Override
    public TaskDAO getTaskDAO() {
        return TaskDAOImplHibernate.getInstance();
    }
}
