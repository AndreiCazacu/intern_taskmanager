package com.stefanini.taskmanager.dao.implHibernate;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.entity.User;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Objects;

public class UserDAOImplHibernate implements UserDAO {
    private static final Logger log = Logger.getLogger(UserDAOImplHibernate.class);
    private static UserDAOImplHibernate instance;
    private Class<User> persistentUser;
    private String className;

    {
        setPersistentClass(User.class);
    }

    private UserDAOImplHibernate() {

    }

    public static synchronized UserDAOImplHibernate getInstance() {
        if (instance == null) {
            instance = new UserDAOImplHibernate();
        }
        return instance;
    }

    @Loggable
    @Override
    public User createRecord(User user, Session session) throws HibernateException {
        log.info("Creating new " + className);
        Long newId = (Long) session.save(user);
        return getRecordById(newId, session);
    }

    @Loggable
    @Override
    public List<User> getRecordsList(Session session) throws HibernateException {
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(persistentUser);
        Root<User> root = criteria.from(persistentUser);
        criteria.select(root);
        return session.createQuery(criteria).getResultList();
    }

    @Loggable
    @Override
    public void deleteAllRecords(Session session) throws HibernateException {
        log.info("Deleting all " + className + " records");
        Query query = session.createQuery("delete from User");
        int rowsAffected = query.executeUpdate();
        if (rowsAffected > 0) {
            log.info("Deleted " + rowsAffected + " records.");
        }
    }

    @Loggable
    @Override
    public User getRecordByUniqueName(String username, Session session) throws HibernateException {
        log.info("Searching for user with username " + username);
        User user;
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(persistentUser);
        Root<User> root = criteria.from(persistentUser);
        criteria.select(root);
        List<User> users = session.createQuery(criteria).getResultList();
        user = users.stream().filter(usr->usr.getUsername().equals(username)).findFirst().orElse(null);
        return user;
    }

    @Loggable
    @Override
    public User deleteRecordByUniqueName(String username, Session session) throws HibernateException {
        User user = getRecordByUniqueName(username, session);
        if (Objects.nonNull(user)) {
            log.info("Deleting " + className + " with username " + username);
            session.delete(user);
            log.info(className + " with username " + username + " was deleted");
        } else {
            log.warn("User " + username + " does not exist.");
        }
        return user;
    }

    @Loggable
    @Override
    public User getRecordById(Long id, Session session) throws HibernateException {
        log.info("Searching for " + className + " with id " + id);
        return session.get(persistentUser, id);
    }

    @Loggable
    @Override
    public User deleteRecordById(Long id, Session session) throws HibernateException {
        User user = getRecordById(id, session);
        if (Objects.nonNull(user)) {
            log.info("Deleting " + className + " with id= " + id);
            session.delete(user);
            log.info(className + " with id=" + id + " was deleted");
        } else {
            log.warn("User with Id= " + id + " does not exist.");
        }
        return user;
    }

    private void setPersistentClass(Class<User> user) {
        this.persistentUser = user;
        this.className = persistentUser == null ? "NoName" : persistentUser.getName();
    }

    private Class<User> getPersistentClass() {
        return persistentUser;
    }
}