package com.stefanini.taskmanager.dao.factory;

import com.stefanini.taskmanager.dao.implHibernate.HibernateDAOFactory;
import com.stefanini.taskmanager.dao.implJDBC.JDBCDAOFactory;

public abstract class AbstractDAOFactory {

    public static DAOFactory createDAOFactory(FactoryType type) throws IllegalArgumentException {

        switch (type) {
            case JDBC:
                return new JDBCDAOFactory();
            case HIBERNATE:
                return new HibernateDAOFactory();
            default:
               throw new IllegalArgumentException(
                       "WARNING: Another factory implementation is not ready yet. Valid types: JDBC or HIBERNATE"); }
    }
}