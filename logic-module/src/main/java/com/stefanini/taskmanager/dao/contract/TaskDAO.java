package com.stefanini.taskmanager.dao.contract;

import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import org.hibernate.Session;

import java.util.List;

public interface TaskDAO extends AbstractDAO<Task> {

    /**
     * Fetches list of tasks assigned to param-user
     *
     * @param username String username-param serving as criteria
     * @param session Hibernate session for Hibernate implementation
     * @return entities list, list of Task instances
     */
    List<Task> getTasksByUsername(String username, Session session);

    /**
     * Assigns param-task to param-user
     *
     * @param user User user-param
     * @param task Task task-param
     * @param session Hibernate session for Hibernate implementation
     */
    void assignTask(User user, Task task, Session session);

    /**
     * Removes param-task from param-user
     *  @param user User user-param
     * @param task Task task-param
     * @param session Hibernate session for Hibernate implementation
     */
    void completeTask(User user, Task task, Session session);

}
