package com.stefanini.taskmanager.dao.implJDBC;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import com.stefanini.taskmanager.util.DBConnector;
import com.stefanini.taskmanager.dao.contract.TaskDAO;
import com.stefanini.taskmanager.dao.contract.UserDAO;
import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import org.apache.log4j.Logger;
import org.hibernate.Session;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskDAOImplJDBC implements TaskDAO {
    private static final Logger log = Logger.getLogger(TaskDAOImplJDBC.class);
    private final UserDAO userDAO = UserDAOImplJDBC.getInstance();
    private static TaskDAOImplJDBC instance;

    private TaskDAOImplJDBC() {

    }

    public static synchronized TaskDAOImplJDBC getInstance() {
        if (instance == null) {
            instance = new TaskDAOImplJDBC();
        }
        return instance;
    }

    @Loggable
    @Override
    public Task createRecord(Task task, Session session) {
        String taskTitle = task.getTaskTitle();
        String taskDescription = task.getTaskDescription();
        String query = "INSERT INTO tasks (Task_Title, Task_Description) VALUES(?,?)";
        long newId = 0L;
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            log.info("Creating new task - " + taskTitle);
            statement.setString(1, taskTitle);
            statement.setString(2, taskDescription);
            statement.executeUpdate();
            try (ResultSet result = statement.getGeneratedKeys()) {
                if (result.next()) {
                    newId = result.getLong(1);
                }
            }
        } catch (SQLException e) {
            log.error("Task was not created due to error - " + e.getMessage());
            e.printStackTrace();
        }
        return getRecordById(newId, session);
    }

    @Loggable
    @Override
    public List<Task> getRecordsList(Session session) {
        String query = "SELECT * FROM tasks";
        List<Task> tasksList = new ArrayList<>();
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    tasksList.add(new Task(result.getLong("Id"),
                            result.getString("Task_Title"),
                            result.getString("Task_Description")));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get all tasks list due to - " + e.getMessage());
            e.printStackTrace();
        }
        return tasksList;
    }

    @Loggable
    @Override
    public void deleteAllRecords(Session session) {
        String query = "DELETE tasks, users_tasks FROM tasks LEFT JOIN users_tasks ON tasks.Id = users_tasks.Id_Task";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error("Can't delete all tasks due to - " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public Task getRecordByUniqueName(String taskTitle, Session session) {
        Task task = null;
        String query = "SELECT * FROM tasks WHERE Task_Title = ?";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, taskTitle);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    task = new Task(result.getLong("Id"),
                            result.getString("Task_Title"),
                            result.getString("Task_Description"));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get task due to - " + e.getMessage());
            e.printStackTrace();
        }
        return task;
    }

    @Loggable
    @Override
    public Task deleteRecordByUniqueName(String taskTitle, Session session) {
        Task task = getRecordByUniqueName(taskTitle, session);
        if (Objects.nonNull(task)) {
            String query = "DELETE FROM tasks WHERE Task_Title= ?";
            try {
                Connection connection = DBConnector.getUniqueConnection();
                PreparedStatement statement = connection.prepareStatement(query);

                deleteLinkUserTask(task, connection);

                statement.setString(1, taskTitle);
                statement.executeUpdate();
                log.info("Task " + taskTitle + " was deleted");
            } catch (SQLException e) {
                log.error("Task was not deleted due to error - " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.warn("Task " + taskTitle + " does not exist.");
        }
        return task;
    }

    @Loggable
    @Override
    public Task getRecordById(Long id, Session session) {
        Task task = null;
        String query = "SELECT * FROM tasks WHERE Id = ?";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setLong(1, id);
            try (ResultSet result = statement.executeQuery()) {
                while (result.next()) {
                    task = new Task(result.getLong("Id"),
                            result.getString("Task_Title"),
                            result.getString("Task_Description"));
                }
            }
        } catch (SQLException e) {
            log.error("Can't get task due to - " + e.getMessage());
            e.printStackTrace();
        }
        return task;
    }

    @Loggable
    @Override
    public Task deleteRecordById(Long id, Session session) {
        Task task = getRecordById(id, session);
        if (Objects.nonNull(task)) {
            String query = "DELETE FROM tasks WHERE Id= ?";
            try {
                Connection connection = DBConnector.getUniqueConnection();
                PreparedStatement statement = connection.prepareStatement(query);

                deleteLinkUserTask(task, connection);

                statement.setLong(1, id);
                statement.executeUpdate();
                log.info("Task with Id= " + id + " was deleted");
            } catch (SQLException e) {
                log.error("Task was not deleted due to error - " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.warn("Task with Id= " + id + " does not exist.");
        }
        return task;
    }

    @Loggable
    @Override
    public List<Task> getTasksByUsername(String username, Session session) {
        User user = userDAO.getRecordByUniqueName(username, session);
        List<Task> userTasks = new ArrayList<>();
        String query = "SELECT t.* FROM tasks AS t JOIN users_tasks AS ut ON t.Id = ut.Id_Task WHERE Id_User= ?";
        if (Objects.nonNull(user)) {
            try {
                Connection connection = DBConnector.getUniqueConnection();
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setLong(1, user.getId());
                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        userTasks.add(new Task(result.getLong("Id"),
                                result.getString("Task_Title"),
                                result.getString("Task_Description")));
                    }
                }
            } catch (SQLException e) {
                log.error("Tasks list was not found due to error - " + e.getMessage());
                e.printStackTrace();
            }
        } else {
            log.warn("User with username " + username + " does not exist.");
        }
        return userTasks;
    }

    @Loggable
    @Override
    public void assignTask(User user, Task task, Session session) {
        String query = "INSERT INTO users_tasks (Id_User, Id_Task) VALUES(?,?)";
        try {
            Connection connection = DBConnector.getUniqueConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setLong(1, user.getId());
            statement.setLong(2, task.getId());
            statement.executeUpdate();
            log.info("Task " + task.getId() + " has been assigned to " + user.getUsername());
        } catch (SQLException e) {
            log.error("Task was not assigned due to error - " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Loggable
    @Override
    public void completeTask(User user, Task task, Session session) {
        List<Task> userTasks = getTasksByUsername(user.getUsername(), session);
        String query = "DELETE FROM users_tasks WHERE Id_User= ? AND Id_Task= ?";
        if (Objects.nonNull(userTasks)) {
            if (userTasks.contains(task)) {
                try {
                    Connection connection = DBConnector.getUniqueConnection();
                    PreparedStatement statement = connection.prepareStatement(query);
                    statement.setLong(1, user.getId());
                    statement.setLong(2, task.getId());
                    statement.executeUpdate();
                    log.info("Task " + task + " was removed from " + user);
                } catch (SQLException e) {
                    log.error("Task was not removed due to error - " + e.getMessage());
                    e.printStackTrace();
                }
            } else {
                log.warn("Task " + task + " does not assign to " + user);        }
        } else {
            log.warn("No tasks assigned to this user!");
        }
    }

    private void deleteLinkUserTask(Task task, Connection connection) {
        String linkQuery = "DELETE FROM users_tasks WHERE Id_Task= ?";
        try {
            PreparedStatement statement = connection.prepareStatement(linkQuery);
            statement.setLong(1, task.getId());
            statement.executeUpdate();
            log.info("Links with users for task " + task + " has been deleted from table users_tasks");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}