package com.stefanini.taskmanager.util;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnector {
    private static final Logger log = Logger.getLogger(DBConnector.class);

    private static String DB_URL, DB_USERNAME, DB_PASSWORD;
    private static Connection uniqueConnection;

    static{
        setConnectionParams();
    }

    private DBConnector() {

    }

    private static void setConnectionParams() {
        Properties properties = AppConfigReader.getProperties();

        DB_URL = properties.getProperty("db.url");
        DB_USERNAME = properties.getProperty("db.username");
        DB_PASSWORD = properties.getProperty("db.password");
    }

    public static Connection getUniqueConnection() {
        try {
            if (uniqueConnection == null) {
            uniqueConnection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
            log.info("Connection launched...");}
        } catch (SQLException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
        return uniqueConnection;
    }
}