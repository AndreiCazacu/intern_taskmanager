package com.stefanini.taskmanager.util;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class AppConfigReader {
    private static final Logger log = Logger.getLogger(AppConfigReader.class);
    private static final String CONFIG_FILE = "appConfig.properties";

    public static Properties getProperties() {
        Properties props = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        try (InputStream stream = loader.getResourceAsStream(CONFIG_FILE)) {
            props.load(stream);
        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }
        return props;
    }
}