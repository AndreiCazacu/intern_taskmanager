package com.stefanini.taskmanager.util;

import com.stefanini.taskmanager.entity.Task;
import com.stefanini.taskmanager.entity.User;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class ArgsReader {
    private static final Logger log = Logger.getLogger(ArgsReader.class);

    public static final String CREATE_NEW_USER = "createUser";
    public static final String CREATE_NEW_TASK = "createTask";
    public static final String SHOW_ALL_USERS = "showAllUsers";
    public static final String SHOW_ALL_TASKS = "showAllTasks";
    public static final String ADD_TASK_TO_USER = "addTask";
    public static final String ADD_NEW_TASK_TO_NEW_USER = "addNewToNew";
    public static final String REMOVE_TASK_FROM_USER = "removeTask";
    public static final String SHOW_USER_TASKS = "showTasks";
    public static final String DELETE_ALL_TASKS = "clearAllTasks";
    public static final String DELETE_TASK = "clearTask";
    public static final String DELETE_ALL_USERS = "clearAllUsers";
    public static final String DELETE_USER = "clearUser";

    public static String UN_LABEL, FN_LABEL, LN_LABEL, TT_LABEL, TD_LABEL;

    static {
        setCommandLineLabels();
    }

    public static void setCommandLineLabels() {
        Properties props = AppConfigReader.getProperties();

        UN_LABEL = props.getProperty("username.label");
        FN_LABEL = props.getProperty("firstname.label");
        LN_LABEL = props.getProperty("lastname.label");
        TT_LABEL = props.getProperty("tasktitle.label");
        TD_LABEL = props.getProperty("taskdescription.label");
    }

    public static String getOperation(String arg) throws IllegalArgumentException {
        String operation = arg.replace("-", "");
        List<String> operations = Arrays.asList(CREATE_NEW_USER, CREATE_NEW_TASK, SHOW_ALL_USERS, SHOW_ALL_TASKS,
                ADD_TASK_TO_USER, ADD_NEW_TASK_TO_NEW_USER, REMOVE_TASK_FROM_USER, SHOW_USER_TASKS,
                DELETE_ALL_TASKS, DELETE_TASK, DELETE_ALL_USERS, DELETE_USER);
        if (!operations.contains(operation)) {
            throw new IllegalArgumentException("Operation does not exist!");
        }
        return operation;
    }

    public static String createParameters(String[] args, String label) {
        StringBuilder strArgs = new StringBuilder();
        for (String arg : args) {
            strArgs.append(arg).append(" ");
        }
        String strParam = strArgs.toString();
        try {
            strParam = strParam.substring(strParam.indexOf(label + "='") + 4);
            strParam = strParam.substring(0, strParam.indexOf("'"));
        } catch (IndexOutOfBoundsException e) {
            log.error("Can not find argument with label: " + label);
            e.printStackTrace();
        }
        return strParam;
    }

    public static Task parseArgsForTask(String[] args) {
        Task task = new Task();
        task.setTaskTitle(createParameters(args, TT_LABEL));
        task.setTaskDescription(createParameters(args, TD_LABEL));
        return task;
    }

    public static User parseArgsForUser(String[] args) {
        User user = new User();
        user.setFirstName(createParameters(args, FN_LABEL));
        user.setLastName(createParameters(args, LN_LABEL));
        user.setUsername(createParameters(args, UN_LABEL));
        return user;
    }
}