package com.stefanini.taskmanager.util;

import com.stefanini.taskmanager.aspect.contract.Loggable;
import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class MailConnector {
    private static final Logger log = Logger.getLogger(MailConnector.class);
    private static final Properties mailProps = AppConfigReader.getProperties();
    private static final Properties mailServerProps = new Properties();
    private static final String MAIL_FROM, MAIL_PASS, MAIL_TO;
    private static final Session session;

    static {
        MAIL_FROM = mailProps.getProperty("mail.from");
        MAIL_PASS = mailProps.getProperty("mail.pass");
        MAIL_TO = mailProps.getProperty("mail.to");
        mailServerProps.put("mail.smtp.starttls.enable", mailProps.getProperty("mail.smtp.starttls.enable"));
        mailServerProps.put("mail.smtp.auth", mailProps.getProperty("mail.smtp.auth"));
        mailServerProps.put("mail.smtp.host", mailProps.getProperty("mail.smtp.host"));
        mailServerProps.put("mail.smtp.port", mailProps.getProperty("mail.smtp.port"));
        mailServerProps.put("mail.smtp.ssl.trust", mailProps.getProperty("mail.smtp.ssl.trust"));

        session = Session.getInstance(mailServerProps,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(MAIL_FROM, MAIL_PASS);
                    }
                });
    }

    @Loggable
    public void sendMail(MimeMessage message) {
        try {
            Transport.send(message);
            log.info("Mail sent!");
        } catch (MessagingException e) {
            log.error("Sending mail error!");
            e.printStackTrace();
        }
    }

    public Session getSession() {
        return session;
    }

    public String getMailTo() {
        return MAIL_TO;
    }

    public String getMailFrom() {
        return MAIL_FROM;
    }

}