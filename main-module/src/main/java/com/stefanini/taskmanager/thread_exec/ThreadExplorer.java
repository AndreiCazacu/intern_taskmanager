package com.stefanini.taskmanager.thread_exec;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadExplorer {
    private static final Logger log = Logger.getLogger(ThreadExplorer.class);
    private final int pool;
    private final List<Runnable> tasks;

    public ThreadExplorer(int pool, List<Runnable> tasks) {
        this.pool = pool;
        this.tasks = tasks;
    }

    public void executeThreadsPool() {
        ExecutorService executor = Executors.newFixedThreadPool(pool);
        try {
            for (Runnable task : tasks) {
                executor.submit(task).get();
                log.info("DONE. THREAD WAS EXECUTED=================================================================");
            }
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            log.error("Thread execution error: " + e.getMessage());
        } finally {
            executor.shutdown();
            log.info("Finished");
        }
    }
}