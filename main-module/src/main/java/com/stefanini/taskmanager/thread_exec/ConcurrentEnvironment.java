package com.stefanini.taskmanager.thread_exec;

import com.stefanini.taskmanager.command_exec.command.*;
import com.stefanini.taskmanager.command_exec.execution.CommandExecution;
import com.stefanini.taskmanager.command_exec.store.CommandStore;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class ConcurrentEnvironment {
    private static final Logger log = Logger.getLogger(ConcurrentEnvironment.class);

    public void receiveArgsAndWorkWithDB(String[] inputs) {
        if (inputs.length == 0) {
            throw new IllegalArgumentException("No params were passed");
        }

        CommandStore commandStore = new CommandStore();
        CommandExecution commandExecution = new CommandExecution(
                new CreateNewUserCmd(commandStore),
                new CreateNewTaskCmd(commandStore),
                new ShowAllUsersCmd(commandStore),
                new ShowAllTasksCmd(commandStore),
                new AddTaskToUserCmd(commandStore),
                new AddNewTaskToNewUserCmd(commandStore),
                new RemoveTaskFromUserCmd(commandStore),
                new ShowUserTasksCmd(commandStore),
                new DeleteAllTasksCmd(commandStore),
                new DeleteTaskCmd(commandStore),
                new DeleteAllUsersCmd(commandStore),
                new DeleteUserCmd(commandStore)
        );

//        List<Runnable> tasks = new ArrayList<>();
//        tasks.add(() -> commandExecution.createNewUser(inputs));
//        tasks.add(() -> commandExecution.createNewTask(inputs));
//        tasks.add(() -> commandExecution.addTaskToUser(inputs));
//        tasks.add(() -> commandExecution.showAllUsers(inputs));
//        tasks.add(() -> commandExecution.showUserTasks(inputs));

        ExecutorService executor = Executors.newFixedThreadPool(5);
        try {
            executor.submit(() -> commandExecution.createNewUser(inputs)).get();
            log.info("DONE. THREAD WAS EXECUTED=================================================================");
            executor.submit(() -> commandExecution.createNewTask(inputs)).get();
            log.info("DONE. THREAD WAS EXECUTED=================================================================");
            executor.submit(() -> commandExecution.addTaskToUser(inputs)).get();
            log.info("DONE. THREAD WAS EXECUTED=================================================================");
            executor.execute(() -> commandExecution.showAllUsers(inputs));
            log.info("DONE. THREAD WAS EXECUTED=================================================================");
            executor.execute(() -> commandExecution.showUserTasks(inputs));
            log.info("DONE. THREAD WAS EXECUTED=================================================================");
            executor.shutdown();
        } catch (InterruptedException | ExecutionException | CancellationException e) {
            log.error("Thread execution error: " + e.getMessage());
        } finally {
            try {
                if (!executor.awaitTermination(1000, TimeUnit.MILLISECONDS)) {
                    executor.shutdownNow();
                }
            } catch (InterruptedException e) {
                executor.shutdownNow();
            }
        }
        log.info("Finished");

//        ThreadExplorer threadExplorer = new ThreadExplorer (tasks.size(), tasks);
//        threadExplorer.executeThreadsPool();
    }
}