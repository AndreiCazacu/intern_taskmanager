package com.stefanini.taskmanager.thread_exec;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ThreadTaskManagerApp {
    private static final Logger log = Logger.getLogger(ThreadTaskManagerApp.class);
    private static final String[] inputs = new String[5];

    public static void main(String[] args) {
        try (BufferedReader rd = new BufferedReader(new InputStreamReader(System.in))) {
            log.info("Enter First_Name");
            inputs[0] = "-fn='" + rd.readLine() + "'";
            log.info("Enter Last_Name");
            inputs[1] = "-ln='" + rd.readLine() + "'";
            log.info("Enter Username");
            inputs[2] = "-un='" + rd.readLine() + "'";
            log.info("Enter Task_Title");
            inputs[3] = "-tt='" + rd.readLine() + "'";
            log.info("Enter Task_Description");
            inputs[4] = "-td='" + rd.readLine() + "'";
        } catch (IOException e) {
            log.error("Error in input: " + e.getMessage());
        }

        ConcurrentEnvironment env = new ConcurrentEnvironment();
        env.receiveArgsAndWorkWithDB(inputs);
    }
}