package com.stefanini.taskmanager.command_exec.command;

import com.stefanini.taskmanager.command_exec.contract.Command;
import com.stefanini.taskmanager.command_exec.store.CommandStore;

public class ShowAllUsersCmd implements Command {
    CommandStore commandStore;

    public ShowAllUsersCmd(CommandStore commandStore) {
        this.commandStore = commandStore;
    }

    @Override
    public void execute(String[] args) {
        commandStore.getUsersList();
    }
}
