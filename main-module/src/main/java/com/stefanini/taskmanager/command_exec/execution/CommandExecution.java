package com.stefanini.taskmanager.command_exec.execution;

import com.stefanini.taskmanager.command_exec.contract.Command;

public class CommandExecution {
    Command createUser;
    Command createTask;
    Command getUsersList;
    Command getTasksList;
    Command addTaskToUser;
    Command addNewTaskToNewUser;
    Command removeTaskFromUser;
    Command getUserTasks;
    Command deleteAllTasks;
    Command deleteTask;
    Command deleteAllUsers;
    Command deleteUser;

    public CommandExecution(
            Command createUser,
            Command createTask,
            Command getUsersList,
            Command getTasksList,
            Command addTaskToUser,
            Command addNewTaskToNewUser,
            Command removeTaskFromUser,
            Command getUserTasks,
            Command deleteAllTasks,
            Command deleteTask,
            Command deleteAllUsers,
            Command deleteUser)
    {
        this.createUser = createUser;
        this.createTask = createTask;
        this.getUsersList = getUsersList;
        this.getTasksList = getTasksList;
        this.addTaskToUser = addTaskToUser;
        this.addNewTaskToNewUser = addNewTaskToNewUser;
        this.removeTaskFromUser = removeTaskFromUser;
        this.getUserTasks = getUserTasks;
        this.deleteAllTasks = deleteAllTasks;
        this.deleteTask = deleteTask;
        this.deleteAllUsers = deleteAllUsers;
        this.deleteUser = deleteUser;
    }

    public void createNewUser(String[] args){
        createUser.execute(args);
    }

    public void createNewTask(String[] args){
        createTask.execute(args);
    }

    public void showAllUsers(String[] args){
        getUsersList.execute(args);
    }

    public void showAllTasks(String[] args){
        getTasksList.execute(args);
    }

    public void addTaskToUser(String[] args){
        addTaskToUser.execute(args);
    }

    public void addNewTaskToNewUser(String[] args){
        addNewTaskToNewUser.execute(args);
    }

    public void removeTaskFromUser(String[] args){
        removeTaskFromUser.execute(args);
    }

    public void showUserTasks(String[] args){
        getUserTasks.execute(args);
    }

    public void deleteAllTasks(String[] args){
        deleteAllTasks.execute(args);
    }

    public void deleteTask(String[] args){
        deleteTask.execute(args);
    }

    public void deleteAllUsers(String[] args){
        deleteAllUsers.execute(args);
    }

    public void deleteUser(String[] args){
        deleteUser.execute(args);
    }
}
