package com.stefanini.taskmanager.command_exec.command;

import com.stefanini.taskmanager.command_exec.contract.Command;
import com.stefanini.taskmanager.command_exec.store.CommandStore;

public class DeleteUserCmd implements Command {
    CommandStore commandStore;

    public DeleteUserCmd(CommandStore commandStore) {
        this.commandStore = commandStore;
    }

    @Override
    public void execute(String[] args) {
        commandStore.deleteUser(args);
    }
}
