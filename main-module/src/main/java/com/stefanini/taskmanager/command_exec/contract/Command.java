package com.stefanini.taskmanager.command_exec.contract;

public interface Command {
    void execute(String[] args);
}
