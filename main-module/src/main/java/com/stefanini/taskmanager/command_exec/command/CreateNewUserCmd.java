package com.stefanini.taskmanager.command_exec.command;

import com.stefanini.taskmanager.command_exec.contract.Command;
import com.stefanini.taskmanager.command_exec.store.CommandStore;

public class CreateNewUserCmd implements Command {
    CommandStore commandStore;

    public CreateNewUserCmd(CommandStore commandStore) {
        this.commandStore = commandStore;
    }

    @Override
    public void execute(String[] args) {
        commandStore.createUser(args);
    }
}
