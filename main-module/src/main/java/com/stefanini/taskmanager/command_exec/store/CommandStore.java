package com.stefanini.taskmanager.command_exec.store;

import com.stefanini.taskmanager.service.contract.TaskService;
import com.stefanini.taskmanager.service.contract.UserService;
import com.stefanini.taskmanager.service.implementation.TaskServiceImpl;
import com.stefanini.taskmanager.service.implementation.UserServiceImpl;

public class CommandStore {
    private static final UserService userService;
    private static final TaskService taskService;

    static {
        userService = new UserServiceImpl();
        taskService = new TaskServiceImpl();
    }

    public void createUser(String[] args) {
        userService.createEntity(args);
    }

    public void createTask(String[] args) {
        taskService.createEntity(args);
    }

    public void getUsersList() {
        userService.getEntityList();
    }

    public void getTasksList() {
        taskService.getEntityList();
    }

    public void addTaskToUser(String[] args) {
        taskService.assignTaskToUser(args);
    }

    public void addNewTaskToNewUser(String[] args) {
        taskService.assignNewTaskToNewUser(args);
    }

    public void removeTaskFromUser(String[] args) {
        taskService.removeTaskFromUser(args);
    }

    public void getUserTasks(String[] args) {
        taskService.getTasksByUsername(args);
    }

    public void deleteAllTasks(String[] args) {
        taskService.deleteAll();
    }

    public void deleteTask(String[] args) {
        taskService.deleteEntity(args);
    }

    public void deleteAllUsers(String[] args) {
        userService.deleteAll();
    }

    public void deleteUser(String[] args) {
        userService.deleteEntity(args);
    }
}
