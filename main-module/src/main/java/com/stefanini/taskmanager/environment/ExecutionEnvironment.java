package com.stefanini.taskmanager.environment;

import com.stefanini.taskmanager.command_exec.command.*;
import com.stefanini.taskmanager.command_exec.execution.CommandExecution;
import com.stefanini.taskmanager.command_exec.store.CommandStore;

import static com.stefanini.taskmanager.util.ArgsReader.*;

public class ExecutionEnvironment {
    public void receiveArgsAndWorkWithDB(String[] args) {
        String operation = "";
        try {
            operation = getOperation(args[0]);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        CommandStore commandStore = new CommandStore();
        CommandExecution commandExecution = new CommandExecution(
                new CreateNewUserCmd(commandStore),
                new CreateNewTaskCmd(commandStore),
                new ShowAllUsersCmd(commandStore),
                new ShowAllTasksCmd(commandStore),
                new AddTaskToUserCmd(commandStore),
                new AddNewTaskToNewUserCmd(commandStore),
                new RemoveTaskFromUserCmd(commandStore),
                new ShowUserTasksCmd(commandStore),
                new DeleteAllTasksCmd(commandStore),
                new DeleteTaskCmd(commandStore),
                new DeleteAllUsersCmd(commandStore),
                new DeleteUserCmd(commandStore)
        );

        switch (operation) {
            case CREATE_NEW_USER: {
                commandExecution.createNewUser(args);
                break;
            }
            case CREATE_NEW_TASK: {
                commandExecution.createNewTask(args);
                break;
            }
            case SHOW_ALL_USERS: {
                commandExecution.showAllUsers(args);
                break;
            }
            case SHOW_ALL_TASKS: {
                commandExecution.showAllTasks(args);
                break;
            }
            case ADD_TASK_TO_USER: {
                commandExecution.addTaskToUser(args);
                break;
            }
            case ADD_NEW_TASK_TO_NEW_USER: {
                commandExecution.addNewTaskToNewUser(args);
                break;
            }
            case REMOVE_TASK_FROM_USER: {
                commandExecution.removeTaskFromUser(args);
                break;
            }
            case SHOW_USER_TASKS: {
                commandExecution.showUserTasks(args);
                break;
            }
            case DELETE_ALL_TASKS: {
                commandExecution.deleteAllTasks(args);
                break;
            }
            case DELETE_TASK: {
                commandExecution.deleteTask(args);
                break;
            }
            case DELETE_ALL_USERS: {
                commandExecution.deleteAllUsers(args);
                break;
            }
            case DELETE_USER: {
                commandExecution.deleteUser(args);
                break;
            }
        }
    }
}