import com.stefanini.taskmanager.environment.ExecutionEnvironment;

public class TaskManagerApp {

    public static void main(String[] args){

        if (args.length == 0) {
            throw new  IllegalArgumentException("No arguments were passed");
        }

        ExecutionEnvironment execution = new ExecutionEnvironment();
        execution.receiveArgsAndWorkWithDB(args);
    }
}